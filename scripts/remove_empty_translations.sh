#!/bin/bash
FOLDER='app/src/main/res/values'
STRINGS='/strings.xml'

for LOCALE in '' '-de'
do

  FILE=${FOLDER}
  FILE+=${LOCALE}
  FILE+=${STRINGS}
  TMP=FILE
  TMP+='.tmp'

  mv ${FILE} ${TMP}

  sed -n '/></!p' ${TMP} > ${FILE}

  rm ${TMP}

done
