#!/bin/bash

printf '\nstart copy config\n'
cp ../configs/project-aeolos/config.gradle app/
cp ../configs/versions.gradle app/
cp ../configs/project-aeolos/google-services.json app/
cp ../configs/android-keystore/foodora-driver-release-key.keystore ./
printf '\nfinished copy config\n'

printf 'start gitlog.sh\n'
./scripts/gitlog.sh
printf 'finished gitlog.sh\n'

printf '========================================================================='

printf '\nstart ./gradlew clean\n'
./gradlew clean
printf '\nfinished ./gradlew clean\n'

printf '========================================================================='

printf '\nstart strings.xml\n'
node scripts/update_localizations.js
./scripts/remove_empty_translations.sh
printf '\nfinished strings.xml\n'

printf '========================================================================='

printf '\nstart zipping\n'
tar cvf secrets.tar app/config.gradle app/google-services.json foodora-driver-release-key.keystore
echo yes | travis encrypt-file -a before_install secrets.tar
rm secrets.tar
printf '\nfinished zipping\n'

printf '========================================================================='

printf '\nstart dependencies\n'
./gradlew dependencyUpdates
printf '\nfinished dependencies'

