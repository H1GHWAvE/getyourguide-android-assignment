package de.neutze.assignment.base.common.extension

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

//region CalendarExtensions

fun String.toCalendar(): Calendar {
    val format = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    val formatter = SimpleDateFormat(format, Locale.getDefault())
    val parsedDate = formatter.parse(this)
    val result = Calendar.getInstance()

    result.time = parsedDate

    return result
}

fun Calendar.toDate(): String {
    val malaysia = Locale.getDefault()
    val df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, malaysia)
    return df.format(this.time)
}

//endregion
