package de.neutze.assignment.base.common.alias

typealias Callback<T> = (T) -> Unit
