package de.neutze.assignment.base.network.exception

class ForbiddenException(override val message: String?) : Throwable()
