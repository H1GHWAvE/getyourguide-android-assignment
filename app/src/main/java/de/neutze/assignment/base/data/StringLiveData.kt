package de.neutze.assignment.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

class StringLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<String>() {

    fun setString(value: String?) {
        executor.execute {
            super.setValue(value)
        }
    }
}
