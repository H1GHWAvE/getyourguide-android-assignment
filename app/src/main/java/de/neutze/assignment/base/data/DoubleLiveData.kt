package de.neutze.assignment.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

class DoubleLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<Double>() {

    fun setDouble(value: Double) {
        executor.execute {
            super.setValue(value)
        }
    }
}
