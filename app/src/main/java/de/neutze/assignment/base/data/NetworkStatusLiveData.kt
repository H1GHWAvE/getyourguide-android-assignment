package de.neutze.assignment.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

class NetworkStatusLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<NetworkStatus>() {

    fun setError(value: NetworkStatus) {
        executor.execute {
            super.setValue(value)
        }
    }
}
