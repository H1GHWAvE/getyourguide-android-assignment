package de.neutze.assignment.base.presentation.snackbar

import android.databinding.ViewDataBinding
import android.support.design.widget.Snackbar
import android.view.View
import com.foodora.courier.aeolos.R
import de.neutze.assignment.base.data.NetworkStatus


fun showStatusSnackbar(binding: ViewDataBinding,
                       networkStatus: NetworkStatus?,
                       clickListener: View.OnClickListener) {

    //TODO (jn): solve in a nicer way than if else AND use exception text
    if (networkStatus != null && networkStatus != NetworkStatus.SUCCESS && networkStatus != NetworkStatus.LOADING) {
        if (networkStatus == NetworkStatus.INVALID) {
            Snackbar.make(binding.root, R.string.all_invalid, Snackbar.LENGTH_SHORT)
                    .show()
        }
        if (networkStatus == NetworkStatus.SUCCESS_CREATED) {
            Snackbar.make(binding.root, R.string.all_created, Snackbar.LENGTH_SHORT).show()
        } else {
            Snackbar.make(binding.root, R.string.all_error, Snackbar.LENGTH_LONG)
                    .setAction(R.string.all_retry, clickListener)
                    .show()
        }
    }
}
