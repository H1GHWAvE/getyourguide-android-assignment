package de.neutze.assignment.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

class IntLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<Int>() {

    fun setInt(value: Int) {
        executor.execute {
            super.setValue(value)
        }
    }
}
