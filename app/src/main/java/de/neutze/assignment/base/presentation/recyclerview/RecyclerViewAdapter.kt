package de.neutze.assignment.base.presentation.recyclerview

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import de.neutze.assignment.base.common.precondition.AndroidPreconditions

class RecyclerViewAdapter(private val factoryMap: Map<Int, ViewHolderFactory>,
                          private val binderMap: Map<Int, ViewHolderBinder>,
                          private val androidPreconditions: AndroidPreconditions) : RecyclerView.Adapter<ViewHolder>() {

    private val modelItems = arrayListOf<DisplayableItem<Any>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        //TODO (JN): ? instead of !!
        return factoryMap[viewType]!!.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = modelItems[position]
        //TODO (JN): ? instead of !!
        binderMap[item.type]!!.bind(holder, item)
    }

    override fun getItemCount(): Int {
        return modelItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return modelItems[position].type
    }

    fun update(items: List<DisplayableItem<Any>>) {
        androidPreconditions.assertUiThread()
        updateAllItems(items)
    }

    private fun updateAllItems(items: List<DisplayableItem<Any>>) {
        updateItemsInModel(items)
        notifyDataSetChanged()
    }

    private fun updateItemsInModel(items: List<DisplayableItem<Any>>) {
        modelItems.clear()
        modelItems.addAll(items)
    }
}
