package de.neutze.assignment.base.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AcceptDialogInterceptor : Interceptor {
    private val headerName = "AcceptDialog"
    private val headerValue = "application/json"

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder().header(headerName, headerValue)

        requestBuilder.method(original.method(), original.body())

        return chain.proceed(requestBuilder.build())
    }

}
