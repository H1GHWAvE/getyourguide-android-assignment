package de.neutze.assignment.base.network.exception

class HttpException(override val message: String?) : Throwable()
