package de.neutze.assignment.base.presentation.binder

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageResource(imageView: ImageView, @DrawableRes resource: Int) {
        imageView.setImageResource(resource)
    }

}
