package de.neutze.assignment.base.presentation.recyclerview

import android.support.v7.widget.RecyclerView.ViewHolder

interface ViewHolderBinder {

    fun bind(viewHolder: ViewHolder, item: DisplayableItem<Any>)

}
