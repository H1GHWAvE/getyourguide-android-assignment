package de.neutze.assignment.base.data

enum class NetworkStatus {
    DISCONNECTED,
    FORBIDDEN,
    HTTP,
    LOADING,
    INVALID,
    PARSE,
    SUCCESS,
    UNAUTHORIZED,
    UNKNOWN,
    SUCCESS_CREATED
}
