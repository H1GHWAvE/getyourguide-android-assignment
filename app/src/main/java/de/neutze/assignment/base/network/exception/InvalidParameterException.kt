package de.neutze.assignment.base.network.exception

data class InvalidParameterException(override val message: String?) : Throwable()
