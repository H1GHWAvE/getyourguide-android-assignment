package de.neutze.assignment.base.network.exception

class UnauthorizedException(override val message: String?) : Throwable()
