package de.neutze.assignment.base.network.exception

data class MissingIdException(override val message: String?) : Throwable()
