package de.neutze.assignment.base.common.mapper

interface Mapper<out OUT, in IN> {
    fun map(input: IN): OUT
}
