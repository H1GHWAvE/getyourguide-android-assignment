package de.neutze.assignment.base.network.interceptor

import de.neutze.assignment.base.network.exception.ForbiddenException
import de.neutze.assignment.base.network.exception.HttpException
import de.neutze.assignment.base.network.exception.UnauthorizedException
import okhttp3.Interceptor
import okhttp3.Response

class HttpErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val code = response.code()

        when (code) {
            401 -> throw UnauthorizedException(message = response.message())
            403 -> throw ForbiddenException(message = response.message())
            else -> {
                if (code !in 200..299) {
                    throw HttpException(message = response.message())
                }
            }
        }

        return response
    }

}

