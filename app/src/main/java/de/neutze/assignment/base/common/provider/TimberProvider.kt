package de.neutze.assignment.base.common.provider

import com.foodora.courier.aeolos.BuildConfig
import timber.log.Timber
import javax.inject.Inject

class TimberProvider
@Inject
internal constructor() {

    fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String {
                    return super.createStackElementTag(element) + ':' + element.lineNumber
                }
            })
        }
    }

}
