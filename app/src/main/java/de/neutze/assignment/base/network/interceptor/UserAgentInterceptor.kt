package de.neutze.assignment.base.network.interceptor

import com.foodora.courier.aeolos.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class UserAgentInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder().header("User-Agent", "Assignment/" + BuildConfig.VERSION_NAME)

        requestBuilder.method(original.method(), original.body())

        return chain.proceed(requestBuilder.build())
    }

}
