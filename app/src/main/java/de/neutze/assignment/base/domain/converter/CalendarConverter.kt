package de.neutze.assignment.base.domain.converter

import android.arch.persistence.room.TypeConverter
import java.util.*

class CalendarConverter {

    @TypeConverter
    fun toCalendar(timestamp: Long): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        return calendar
    }

    @TypeConverter
    fun toTimestamp(calendar: Calendar): Long = calendar.timeInMillis

}
