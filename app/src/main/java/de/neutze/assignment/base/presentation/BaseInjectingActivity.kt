package de.neutze.assignment.base.presentation

import android.os.Bundle
import android.support.annotation.CallSuper
import de.neutze.assignment.base.common.precondition.Preconditions

abstract class BaseInjectingActivity<Component> : BaseActivity() {

    private var component: Component? = null

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        component = createComponent()
        onInject(component!!)

        super.onCreate(savedInstanceState)
    }

    fun getComponent(): Component {
        return Preconditions[component]
    }

    protected abstract fun onInject(component: Component)

    protected abstract fun createComponent(): Component

}
