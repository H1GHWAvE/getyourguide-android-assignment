package de.neutze.assignment.base.presentation

import android.content.Context
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment

abstract class BaseInjectingFragment : Fragment() {

    @CallSuper
    override fun onAttach(context: Context) {
        onInject()

        super.onAttach(context)
    }

    abstract fun onInject()

}
