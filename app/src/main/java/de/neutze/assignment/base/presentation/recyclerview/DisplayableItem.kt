package de.neutze.assignment.base.presentation.recyclerview

data class DisplayableItem<T>(var type: Int,
                              var model: T)
