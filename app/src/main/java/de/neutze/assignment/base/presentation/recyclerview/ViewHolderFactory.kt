package de.neutze.assignment.base.presentation.recyclerview

import android.content.Context
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup

abstract class ViewHolderFactory protected constructor(protected val context: Context) {

    abstract fun createViewHolder(parent: ViewGroup): ViewHolder

}
