package de.neutze.assignment.base.presentation.provider

import android.support.annotation.StringRes
import de.neutze.assignment.app.application.AssignmentApplication
import javax.inject.Inject

class StringProvider
@Inject
internal constructor(private val app: AssignmentApplication) {

    fun getString(@StringRes resId: Int): String {
        return app.getString(resId)
    }

}
