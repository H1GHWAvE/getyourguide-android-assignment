package de.neutze.assignment.browse.domain.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "reviews")
data class Review(@PrimaryKey val reviewId: Long,
                  val author: String,
                  val date: String,
                  //TODO (jn): add converter
                  // val dateUnformatted: Objects,
                  val foreignLanguage: Boolean,
                  val languageCode: String,
                  val message: String,
                  val rating: String,
                  val reviewerCountry: String,
                  val reviewerName: String,
                  val title: String,
                  val traveler_type: String)
