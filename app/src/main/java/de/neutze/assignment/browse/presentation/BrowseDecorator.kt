package de.neutze.assignment.browse.presentation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.databinding.FragmentBrowseBinding
import de.neutze.assignment.base.data.NetworkStatus
import de.neutze.assignment.base.presentation.BaseActivity
import de.neutze.assignment.base.presentation.recyclerview.RecyclerViewAdapter
import de.neutze.assignment.base.presentation.snackbar.showStatusSnackbar
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.presentation.review.AlertDialogCreator
import de.neutze.assignment.browse.presentation.reviews.ReviewDisplayableItemMapper
import javax.inject.Inject

class BrowseDecorator
@Inject
constructor(private val activity: BaseActivity,
            private val alertDialogCreator: AlertDialogCreator,
            private val reviewDisplayableItemMapper: ReviewDisplayableItemMapper,
            private val recyclerAdapter: RecyclerViewAdapter) : BrowseUserInterface {

    //region ButterKnife

    @BindView(R.id.recycler_fragment_browse_reviews)
    lateinit var countryRecyclerView: RecyclerView

    //endregion


    //region var

    private lateinit var binding: FragmentBrowseBinding
    private lateinit var viewModel: BrowseViewModel

    private var delegate: BrowseUserInterface.Delegate? = null

    //endregion


    //region init

    fun bind(binding: FragmentBrowseBinding) {
        ButterKnife.bind(this, binding.root)
        this.binding = binding

        configureReviewRecyclerView()
    }

    override fun initialize(delegate: BrowseUserInterface.Delegate,
                            viewModel: BrowseViewModel) {

        this.delegate = delegate
        this.viewModel = viewModel

        binding.delegate = delegate
        binding.viewModel = viewModel
        binding.empty = true

        setReviewsObserver(viewModel.reviews)
        setStatusObserver()
    }

    fun dispose() {
        delegate = null
    }

    //endregion


    //region observer

    private fun setReviewsObserver(reviews: LiveData<List<Review>>) {
        reviews.observe(activity, Observer {
            updateReviewsRecycler(it)

        })
    }

    private fun setStatusObserver() {
        viewModel.networkStatus.observe(activity, Observer {
            setStatus(it)
        })
    }

    //endregion


    //region reviews

    private fun configureReviewRecyclerView() {
        countryRecyclerView.adapter = recyclerAdapter
        countryRecyclerView.layoutManager = LinearLayoutManager(activity)
        countryRecyclerView.setHasFixedSize(true)
    }

    private fun updateReviewsRecycler(reviews: List<Review>?) {
        recyclerAdapter.update(reviewDisplayableItemMapper.map(reviews))
        setEmpty(reviews == null || reviews.isEmpty())
    }

    //endregion


    //region empty

    private fun setEmpty(empty: Boolean) {
        binding.empty = empty
    }

    //endregion


    //region status

    private fun setStatus(networkStatus: NetworkStatus?) {
        binding.status = networkStatus
        showSnackbar(networkStatus)
    }

    //endregion


    //region Snackbar

    private fun showSnackbar(networkStatus: NetworkStatus?) {
        val clickListener = View.OnClickListener { delegate?.onRefresh() }

        showStatusSnackbar(binding, networkStatus, clickListener)
    }

    //endregion


    //region dialog

    override fun openReviewEditor() {
        alertDialogCreator.createReviewEditor(delegate).show()
    }

    //endregion

}
