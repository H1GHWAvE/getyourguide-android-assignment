package de.neutze.assignment.browse.injection.module

import dagger.Module
import dagger.Provides
import de.neutze.assignment.base.data.NetworkStatusLiveData
import de.neutze.assignment.browse.data.ReviewDownloadUseCase
import de.neutze.assignment.browse.data.ReviewRepository
import de.neutze.assignment.browse.data.ReviewUploadUseCase
import de.neutze.assignment.browse.domain.db.ReviewDao
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@Module
internal class BrowseDataModule {

    @Singleton
    @Provides
    fun provideEndpointRepository(reviewDao: ReviewDao,
                                  reviewDownloadUseCase: ReviewDownloadUseCase,
                                  reviewUploadUseCase: ReviewUploadUseCase,
                                  statusLiveData: NetworkStatusLiveData): ReviewRepository {

        return ReviewRepository(
                reviewDao,
                reviewDownloadUseCase,
                reviewUploadUseCase,
                statusLiveData
        )
    }

}
