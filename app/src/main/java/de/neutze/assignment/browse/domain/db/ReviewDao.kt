package de.neutze.assignment.browse.domain.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import de.neutze.assignment.browse.domain.entities.Review

@Dao
abstract class ReviewDao {

    //region insert

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(review: Review)

    fun insert(reviews: List<Review>) {
        for (review in reviews) {
            insert(review)
        }
    }

    //endregion


    //region delete

    @Query("DELETE FROM reviews")
    abstract fun clear()

    //endregion


    //region select

    @Query("SELECT * FROM reviews")
    abstract fun getReviewList(): LiveData<List<Review>>

    //endregion

}
