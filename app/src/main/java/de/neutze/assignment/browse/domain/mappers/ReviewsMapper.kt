package de.neutze.assignment.browse.domain.mappers

import de.neutze.assignment.base.common.mapper.Mapper
import de.neutze.assignment.base.network.exception.InvalidParameterException
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.network.model.ServerResponse
import javax.inject.Inject

class ReviewsMapper
@Inject
constructor(private val reviewMapper: ReviewMapper) : Mapper<List<Review>, ServerResponse?> {

    override fun map(input: ServerResponse?): List<Review> {
        if (input?.status == false) {
            throw InvalidParameterException(input.message)
        }

        return input?.data.let {
            it?.map {
                reviewMapper.map(it)
            } ?: listOf()
        }
    }

}
