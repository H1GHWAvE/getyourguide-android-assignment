package de.neutze.assignment.browse.data


import com.foodora.courier.aeolos.BuildConfig
import de.neutze.assignment.base.common.alias.Callback
import de.neutze.assignment.base.data.NetworkStatus
import de.neutze.assignment.browse.domain.db.ReviewDao
import de.neutze.assignment.browse.domain.mappers.ReviewsMapper
import de.neutze.assignment.browse.network.service.GYGService
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.Executor
import javax.inject.Inject

class ReviewDownloadUseCase
@Inject
constructor(private val reviewsMapper: ReviewsMapper,
            private val reviewDao: ReviewDao,
            private val executor: Executor,
            private val httpClient: OkHttpClient) {

    fun get(query: Map<String, String>,
            callback: Callback<NetworkStatus>) {

        callback(NetworkStatus.LOADING)

        executor.execute {
            try {
                val gygClient = retrofit2.Retrofit.Builder()
                        .client(httpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BuildConfig.URL_BASE)
                        .build()
                        .create(GYGService::class.java)

                val response = gygClient.getReviews(BuildConfig.URL_JSON, query).execute()
                val body = response.body()
                val reviews = reviewsMapper.map(body)

                reviewDao.clear()
                reviewDao.insert(reviews)

                callback(NetworkStatus.SUCCESS)
            } catch (e: Throwable) {
                when (e) {
                    is InvalidPropertiesFormatException -> callback(NetworkStatus.INVALID)
                    else -> callback(NetworkStatus.UNKNOWN)
                }
            }
        }
    }

}
