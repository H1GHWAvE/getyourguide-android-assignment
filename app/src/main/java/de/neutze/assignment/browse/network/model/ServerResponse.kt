package de.neutze.assignment.browse.network.model

import com.google.gson.annotations.SerializedName

data class ServerResponse(@SerializedName("status") val status: Boolean,
                          @SerializedName("total_reviews_comments") val totalReviewsComments: String,
                          @SerializedName("message") val message: String,
                          @SerializedName("data") val data: List<ReviewResponse>)
