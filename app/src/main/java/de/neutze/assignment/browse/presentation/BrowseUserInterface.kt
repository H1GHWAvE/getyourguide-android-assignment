package de.neutze.assignment.browse.presentation

import de.neutze.assignment.browse.domain.entities.ReviewPost

interface BrowseUserInterface {

    fun initialize(delegate: Delegate,
                   viewModel: BrowseViewModel)

    interface Delegate {

        fun onCreate(review: ReviewPost)

        fun onEdit()

        fun onFilter()

        fun onRefresh()

    }

    fun openReviewEditor()

}
