package de.neutze.assignment.browse.presentation

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.databinding.FragmentBrowseBinding
import de.neutze.assignment.base.presentation.BaseInjectingActivity
import de.neutze.assignment.base.presentation.BaseInjectingFragment
import de.neutze.assignment.browse.injection.component.BrowseFragmentComponent
import javax.inject.Inject

class BrowseFragment : BaseInjectingFragment() {
    
    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var decorator: BrowseDecorator
    @Inject
    internal lateinit var presenter: BrowsePresenter

    private lateinit var binding: FragmentBrowseBinding
    private lateinit var viewModel: BrowseViewModel


    //region lifecycle

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_browse, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BrowseViewModel::class.java)

        decorator.bind(binding)
        presenter.initialize(decorator, viewModel)

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
        decorator.dispose()
    }

    //endregion


    //region extend

    override fun onInject() {
        val activity = BaseInjectingActivity::class.java.cast(activity)
        val componentCreator = BrowseFragmentComponent.BrowseComponentCreator::class.java.cast(activity.getComponent())
        componentCreator.createBrowseFragmentComponent().inject(this)
    }

    //endregion

}
