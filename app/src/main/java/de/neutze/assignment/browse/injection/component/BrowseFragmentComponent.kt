package de.neutze.assignment.browse.injection.component

import dagger.Subcomponent
import de.neutze.assignment.base.injection.scope.FragmentScope
import de.neutze.assignment.browse.injection.module.BrowseFragmentModule
import de.neutze.assignment.browse.presentation.BrowseFragment

@FragmentScope
@Subcomponent(modules = [(BrowseFragmentModule::class)])
interface BrowseFragmentComponent {

    fun inject(fragment: BrowseFragment)

    interface BrowseComponentCreator {

        fun createBrowseFragmentComponent(): BrowseFragmentComponent
    }

}
