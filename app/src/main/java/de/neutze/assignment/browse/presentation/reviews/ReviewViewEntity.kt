package de.neutze.assignment.browse.presentation.reviews

data class ReviewViewEntity(val title: String)
