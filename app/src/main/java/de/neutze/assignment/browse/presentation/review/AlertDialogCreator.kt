package de.neutze.assignment.browse.presentation.review

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.AppCompatEditText
import android.view.LayoutInflater
import android.widget.RatingBar
import com.foodora.courier.aeolos.R
import de.neutze.assignment.base.presentation.provider.StringProvider
import de.neutze.assignment.browse.domain.entities.ReviewPost
import de.neutze.assignment.browse.presentation.BrowseUserInterface
import javax.inject.Inject


class AlertDialogCreator
@Inject
constructor(private val context: Context,
            private val stringProvider: StringProvider) {

    fun createReviewEditor(delegate: BrowseUserInterface.Delegate?): AlertDialog {

        val dialogBuilder = AlertDialog.Builder(context)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_browse_editor, null)

        dialogBuilder.setView(dialogView)

        val message = dialogView.findViewById(R.id.edittext_dialog_browse_editor_message) as AppCompatEditText
        val rating = dialogView.findViewById(R.id.ratingbar_dialog_browser_editor) as RatingBar
        val title = dialogView.findViewById(R.id.edittext_dialog_browse_editor_title) as AppCompatEditText

        dialogBuilder.setTitle(stringProvider.getString(R.string.browse_review_create))
        dialogBuilder.setPositiveButton(
                stringProvider.getString(R.string.all_create),
                { _, _ ->
                    delegate?.onCreate(
                            ReviewPost(
                                    message = message.text.toString(),
                                    rating = rating.rating.toString(),
                                    title = title.text.toString()
                            )
                    )
                }
        )
        dialogBuilder.setNegativeButton(
                stringProvider.getString(R.string.all_cancel),
                { _, _ -> }
        )

        return dialogBuilder.create()
    }
}
