package de.neutze.assignment.browse.injection.module;

import java.util.Map;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import de.neutze.assignment.base.common.precondition.AndroidPreconditions;
import de.neutze.assignment.base.presentation.recyclerview.RecyclerViewAdapter;
import de.neutze.assignment.base.presentation.recyclerview.ViewHolderBinder;
import de.neutze.assignment.base.presentation.recyclerview.ViewHolderFactory;
import de.neutze.assignment.browse.presentation.BrowseConstants;
import de.neutze.assignment.browse.presentation.reviews.ReviewViewHolder;

@Module
public abstract class BrowseFragmentModule {

    @Provides
    static RecyclerViewAdapter provideRecyclerAdapter(Map<Integer, ViewHolderFactory> factoryMap,
                                                      Map<Integer, ViewHolderBinder> binderMap,
                                                      AndroidPreconditions androidPreconditions) {
        return new RecyclerViewAdapter(factoryMap, binderMap, androidPreconditions);
    }

    @Binds
    @IntoMap
    @IntKey(BrowseConstants.DisplayableTypes.REVIEW)
    abstract ViewHolderFactory provideReviewViewHolderFactory(ReviewViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(BrowseConstants.DisplayableTypes.REVIEW)
    abstract ViewHolderBinder provideReviewViewHolderBinder(ReviewViewHolder.Binder binder);

}
