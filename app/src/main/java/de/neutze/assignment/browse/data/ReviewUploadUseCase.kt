package de.neutze.assignment.browse.data


import com.foodora.courier.aeolos.BuildConfig
import de.neutze.assignment.base.common.alias.Callback
import de.neutze.assignment.base.data.NetworkStatus
import de.neutze.assignment.browse.domain.db.ReviewDao
import de.neutze.assignment.browse.domain.entities.ReviewPost
import de.neutze.assignment.browse.domain.mappers.ReviewMapper
import de.neutze.assignment.browse.network.mapper.ReviewResponseMapper
import de.neutze.assignment.browse.network.service.GYGService
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.*
import java.util.concurrent.Executor
import javax.inject.Inject

class ReviewUploadUseCase
@Inject
constructor(private val reviewResponseMapper: ReviewResponseMapper,
            private val reviewMapper: ReviewMapper,
            private val reviewDao: ReviewDao,
            private val executor: Executor,
            private val httpClient: OkHttpClient) {

    fun post(review: ReviewPost,
             callback: Callback<NetworkStatus>) {

        callback(NetworkStatus.LOADING)

        executor.execute {
            try {
                val gygClient = retrofit2.Retrofit.Builder()
                        .client(httpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BuildConfig.URL_BASE)
                        .build()
                        .create(GYGService::class.java)

                val reviewResponse = reviewResponseMapper.map(review)

                val response = gygClient.postReview(reviewResponse).execute()

                val body = response.body()
                val reviews = reviewMapper.map(body)

                reviewDao.insert(reviews)

                callback(NetworkStatus.SUCCESS_CREATED)
            } catch (e: Throwable) {
                Timber.e("%s", e)
                when (e) {
                    is InvalidPropertiesFormatException -> callback(NetworkStatus.INVALID)
                    else -> callback(NetworkStatus.UNKNOWN)
                }
            }
        }
    }

}
