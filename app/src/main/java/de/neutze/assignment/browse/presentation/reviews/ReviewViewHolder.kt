package de.neutze.assignment.browse.presentation.reviews

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foodora.courier.aeolos.BR
import com.foodora.courier.aeolos.R
import de.neutze.assignment.base.presentation.recyclerview.DisplayableItem
import de.neutze.assignment.base.presentation.recyclerview.ViewHolderBinder
import de.neutze.assignment.base.presentation.recyclerview.ViewHolderFactory
import javax.inject.Inject


internal class ReviewViewHolder
private constructor(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    private fun bind(review: ReviewViewEntity) {
        binding.invalidateAll()
        binding.setVariable(BR.review, review)
        binding.executePendingBindings()
    }

    internal class Factory
    @Inject
    constructor(context: Context) : ViewHolderFactory(context) {

        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ViewDataBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.viewholder_browse_review,
                    parent,
                    false
            )
            return ReviewViewHolder(binding)
        }
    }

    internal class Binder
    @Inject
    constructor() : ViewHolderBinder {
        override fun bind(viewHolder: RecyclerView.ViewHolder, item: DisplayableItem<Any>) {
            val castedViewHolder = ReviewViewHolder::class.java.cast(viewHolder)
            val viewEntity = ReviewViewEntity::class.java.cast(item.model)
            castedViewHolder.bind(viewEntity)
        }
    }
}
