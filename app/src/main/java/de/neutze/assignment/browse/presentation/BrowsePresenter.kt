package de.neutze.assignment.browse.presentation

import de.neutze.assignment.browse.domain.entities.ReviewPost
import timber.log.Timber
import javax.inject.Inject

class BrowsePresenter
@Inject
constructor() {

    private lateinit var viewModel: BrowseViewModel
    private var decorator: BrowseUserInterface? = null

    //region delegate

    private val delegate = object : BrowseUserInterface.Delegate {

        override fun onCreate(review : ReviewPost) {
            Timber.e("%s",review)
            viewModel.createReview(review)
        }

        override fun onEdit() {
            decorator?.openReviewEditor()
        }

        override fun onFilter() {
            viewModel.filterReviews()
        }

        override fun onRefresh() {
            viewModel.refreshReviews()
        }

    }

    //endregion


    //region lifecycle

    fun initialize(decorator: BrowseUserInterface,
                   viewModel: BrowseViewModel) {

        this.decorator = decorator
        this.viewModel = viewModel
        this.decorator?.initialize(delegate, viewModel)
    }

    fun dispose() {
        this.decorator = null
    }

    //endregion

}
