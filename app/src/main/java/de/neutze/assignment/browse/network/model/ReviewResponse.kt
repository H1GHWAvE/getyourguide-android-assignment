package de.neutze.assignment.browse.network.model

import com.google.gson.annotations.SerializedName

data class ReviewResponse(@SerializedName("review_id") var reviewId: Long? = null,
                          @SerializedName("rating") val rating: String? = null,
                          @SerializedName("title") val title: String? = null,
                          @SerializedName("message") val message: String? = null,
                          @SerializedName("author") val author: String? = null,
                          @SerializedName("foreignLanguage") val foreignLanguage: Boolean? = null,
                          @SerializedName("date") val date: String? = null,
                          @SerializedName("date_unformatted") val dateUnformatted: Any? = null,
                          @SerializedName("languageCode") val languageCode: String? = null,
                          @SerializedName("traveler_type") val traveler_type: String? = null,
                          @SerializedName("reviewerName") val reviewerName: String? = null,
                          @SerializedName("reviewerCountry") val reviewerCountry: String? = null)
