package de.neutze.assignment.browse.network.mapper

import de.neutze.assignment.base.common.mapper.Mapper
import de.neutze.assignment.browse.domain.entities.ReviewPost
import de.neutze.assignment.browse.network.model.ReviewResponse
import javax.inject.Inject

class ReviewResponseMapper
@Inject
constructor() : Mapper<ReviewResponse, ReviewPost> {

    override fun map(input: ReviewPost): ReviewResponse {

        return input.let {
            ReviewResponse(
                    message = it.message,
                    rating = it.rating,
                    title = it.title
            )
        }
    }

}
