package de.neutze.assignment.browse.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import de.neutze.assignment.base.presentation.util.ViewModelUtil
import de.neutze.assignment.browse.presentation.BrowseViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@Module
class BrowseViewModelModule {

    @Singleton
    @Provides
    internal fun provideViewModelProviderFactory(viewModelUtil: ViewModelUtil,
                                                 viewModel: BrowseViewModel):
            ViewModelProvider.Factory {

        return viewModelUtil.createFor<ViewModel>(viewModel)
    }
}
