package de.neutze.assignment.browse.domain.mappers

import de.neutze.assignment.base.common.mapper.Mapper
import de.neutze.assignment.base.network.exception.MissingIdException
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.network.model.ReviewResponse
import timber.log.Timber
import javax.inject.Inject

class ReviewMapper
@Inject
constructor() : Mapper<Review, ReviewResponse?> {

    @Throws(NullPointerException::class)
    override fun map(input: ReviewResponse?): Review {
        Timber.e("%s", input)
        return input?.let {
            Review(
                    reviewId = input.reviewId ?: throw MissingIdException("id missing"),
                    author = input.author ?: "empty",
                    date = input.date ?: "empty",
                    //TODO (jn): add converter
                    //dateUnformatted = input.dateUnformatted,
                    foreignLanguage = input.foreignLanguage ?: false,
                    languageCode = input.languageCode ?: "empty",
                    message = input.message ?: "empty",
                    rating = input.rating ?: "empty",
                    reviewerCountry = input.reviewerCountry ?: "empty",
                    reviewerName = input.reviewerName ?: "empty",
                    title = input.title ?: "empty",
                    traveler_type = input.traveler_type ?: "empty"
            )
        } ?: throw NullPointerException()
    }

}
