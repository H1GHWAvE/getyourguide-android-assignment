package de.neutze.assignment.browse.domain.entities

data class ReviewPost(val message: String,
                      val rating: String,
                      val title: String)
