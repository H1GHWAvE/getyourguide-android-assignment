package de.neutze.assignment.browse.presentation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import de.neutze.assignment.base.data.NetworkStatus
import de.neutze.assignment.browse.data.ReviewRepository
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.domain.entities.ReviewPost
import javax.inject.Inject

class BrowseViewModel
@Inject
constructor(private val reviewRepository: ReviewRepository) : ViewModel() {

    val reviews: LiveData<List<Review>>
    val networkStatus: LiveData<NetworkStatus>

    val queryCount: ObservableField<String>
    //TODO (jn): improve StringDef/enum
    val queryDirection: ObservableField<String>
    val queryPage: ObservableField<String>
    val queryRating: ObservableField<String>
    //TODO (jn): improve StringDef/enum
    val querySortBy: ObservableField<String>

    val reviewMessage: ObservableField<String>
    val reviewRating: ObservableField<String>
    val reviewTitle: ObservableField<String>

    init {
        queryCount = ObservableField("5")
        queryDirection = ObservableField("DESC")
        queryPage = ObservableField("0")
        queryRating = ObservableField("0.0")
        querySortBy = ObservableField("date_of_review")

        reviewMessage = ObservableField("")
        reviewRating = ObservableField("")
        reviewTitle = ObservableField("")

        reviews = initReviews()
        networkStatus = initStatus()
    }


    //region reviews

    fun refreshReviews() {
        downloadReviews()
    }

    fun filterReviews() {
        downloadReviews()
    }

    private fun initReviews(): LiveData<List<Review>> {
        return reviewRepository.getReviews(getQuery())
    }

    private fun downloadReviews() {
        reviewRepository.downloadReviews(getQuery())
    }

    //endregion


    //region query

    private fun getQuery(): Map<String, String> {
        val query = mutableMapOf<String, String>()

        queryCount.get()?.let { query["count"] = it }
        queryPage.get()?.let { query["page"] = it }
        queryRating.get()?.let { query["rating"] = it }
        querySortBy.get()?.let { query["sortBy"] = it }
        queryDirection.get()?.let { query["direction"] = it }

        return query
    }

    //endregion


    //region networkStatus

    private fun initStatus(): LiveData<NetworkStatus> {
        return reviewRepository.getStatus()
    }

    fun createReview(review: ReviewPost) {
        reviewRepository.uploadReview(review)
    }

    //endregion

}
