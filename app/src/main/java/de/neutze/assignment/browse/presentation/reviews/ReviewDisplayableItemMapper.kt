package de.neutze.assignment.browse.presentation.reviews

import de.neutze.assignment.base.common.mapper.Mapper
import de.neutze.assignment.base.presentation.recyclerview.DisplayableItem
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.presentation.BrowseConstants
import javax.inject.Inject

class ReviewDisplayableItemMapper
@Inject
constructor() : Mapper<List<DisplayableItem<Any>>, List<Review>?> {

    override fun map(input: List<Review>?): List<DisplayableItem<Any>> {
        return input?.let {
            it.map {
                DisplayableItem(
                        type = BrowseConstants.DisplayableTypes.REVIEW,
                        model =
                        ReviewViewEntity(
                                title = it.title
                        ) as Any
                )
            }
        } ?: listOf()
    }

}
