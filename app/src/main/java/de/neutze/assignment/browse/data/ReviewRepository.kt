package de.neutze.assignment.browse.data

import android.arch.lifecycle.LiveData
import de.neutze.assignment.base.data.NetworkStatus
import de.neutze.assignment.base.data.NetworkStatusLiveData
import de.neutze.assignment.browse.domain.db.ReviewDao
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.domain.entities.ReviewPost

class ReviewRepository
constructor(private val reviewDao: ReviewDao,
            private val reviewDownloadUseCase: ReviewDownloadUseCase,
            private val reviewUploadUseCase: ReviewUploadUseCase,
            private val statusData: NetworkStatusLiveData) {


    //region ReviewDao

    fun getReviews(query: Map<String, String>): LiveData<List<Review>> {
        downloadReviews(query)
        return reviewDao.getReviewList()
    }

    //endregion


    //region ReviewDownloadUseCase

    fun downloadReviews(query: Map<String, String>) {
        reviewDownloadUseCase.get(query, { statusData.setError(it) })
    }

    //endregion


    //region ReviewUploadUseCase

    fun uploadReview(review: ReviewPost) {
        reviewUploadUseCase.post(review, { statusData.setError(it) })
    }


    //endregion


    //region NetworkStatus

    fun getStatus(): LiveData<NetworkStatus> = statusData

    //endregion

}
