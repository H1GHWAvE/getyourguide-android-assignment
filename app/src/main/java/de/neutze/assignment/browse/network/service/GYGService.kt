package de.neutze.assignment.browse.network.service

import de.neutze.assignment.browse.network.model.ReviewResponse
import de.neutze.assignment.browse.network.model.ServerResponse
import retrofit2.Call
import retrofit2.http.*

interface GYGService {

    @GET("{reviewJson}")
    fun getReviews(@Path("reviewJson") countrySelectionJson: String,
                   @QueryMap params: Map<String, String>): Call<ServerResponse>

    @POST("post")
    fun postReview(@Body reviewResponse: ReviewResponse): Call<ReviewResponse>

}
