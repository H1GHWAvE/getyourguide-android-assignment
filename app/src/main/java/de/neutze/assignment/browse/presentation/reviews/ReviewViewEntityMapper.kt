package de.neutze.assignment.browse.presentation.reviews

import de.neutze.assignment.base.common.mapper.Mapper
import de.neutze.assignment.browse.presentation.BrowseUserInterface
import javax.inject.Inject

class ReviewViewEntityMapper
@Inject
constructor() : Mapper<List<ReviewViewEntity>, List<String>> {

    private lateinit var delegate: BrowseUserInterface.Delegate

    override fun map(input: List<String>): List<ReviewViewEntity> {
        return listOf()
    }
}
