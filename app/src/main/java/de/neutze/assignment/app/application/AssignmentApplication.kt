package de.neutze.assignment.app.application

import android.app.Application
import android.support.annotation.CallSuper
import de.neutze.assignment.base.common.provider.TimberProvider
import javax.inject.Inject

class AssignmentApplication : Application() {

    @Inject
    lateinit var timberProvider: TimberProvider

    var component: ApplicationComponent? = null

    //region


    //region lifecycle

    @CallSuper
    override fun onCreate() {
        super.onCreate()
        getApplicationComponent()?.inject(this)

        timberProvider.init()
    }

    //endregion


    //region extend

    private fun getApplicationComponent(): ApplicationComponent? {
        if (component == null) {
            component = DaggerApplicationComponent.builder().application(this).build()
        }
        return component
    }

    //endregion

}
