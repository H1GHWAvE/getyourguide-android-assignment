package de.neutze.assignment.app.home.presentation

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.foodora.courier.aeolos.R
import de.neutze.assignment.app.application.AssignmentApplication
import de.neutze.assignment.base.injection.module.ActivityModule
import de.neutze.assignment.base.presentation.BaseInjectingActivity
import de.neutze.assignment.browse.presentation.BrowseFragment

class HomeActivity : BaseInjectingActivity<HomeActivityComponent>() {

    override val activity: Activity
        get() = this
    override val context: Context
        get() = this

    //region lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        showLoginFragment()
    }

    //endregion


    //region extend

    override fun onInject(component: HomeActivityComponent) {
        createComponent().inject(this)
    }

    override fun createComponent(): HomeActivityComponent {
        val app = AssignmentApplication::class.java.cast(application)
        val activityModule = ActivityModule(this)
        return app.component!!.createHomeActivityComponent(activityModule)
    }

    //endregion


    //region fragments

    private fun showLoginFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.frame_home_fragment, BrowseFragment())
                .commitNow()
    }

    //endregion

}
