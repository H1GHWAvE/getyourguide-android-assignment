package de.neutze.assignment.app.application

import android.app.Application
import android.arch.persistence.room.Room
import de.neutze.assignment.app.home.domain.db.Database
import de.neutze.assignment.browse.domain.db.ReviewDao
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.09.17.
 */

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideDb(application: Application): Database = Room.databaseBuilder(application, Database::class.java, "roadrunner.db")
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun provideCountriesDao(db: Database): ReviewDao = db.reviewsDao()

    @Singleton
    @Provides
    fun provideExecutor(): Executor = Executors.newSingleThreadExecutor()

}
