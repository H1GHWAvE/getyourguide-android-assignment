package de.neutze.assignment.app.application

import com.foodora.courier.aeolos.BuildConfig
import dagger.Module
import dagger.Provides
import de.neutze.assignment.app.mock.MockInterceptor
import de.neutze.assignment.base.network.interceptor.AcceptDialogInterceptor
import de.neutze.assignment.base.network.interceptor.HttpErrorInterceptor
import de.neutze.assignment.base.network.interceptor.UserAgentInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class NetworkModule {

    @Provides
    fun provideAcceptDialogInterceptor(): AcceptDialogInterceptor {
        return AcceptDialogInterceptor()
    }

    @Provides
    fun provideUserAgentInterceptor(): UserAgentInterceptor {
        return UserAgentInterceptor()
    }

    @Provides
    fun provideHttpErrorInterceptor(): HttpErrorInterceptor {
        return HttpErrorInterceptor()
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }


    @Provides
    fun provideMockInterceptor(): MockInterceptor {
        return MockInterceptor()
    }


    @Provides
    fun provideOkHttpClient(acceptDialogInterceptor: AcceptDialogInterceptor,
                            userAgentInterceptor: UserAgentInterceptor,
                            mockInterceptor: MockInterceptor,
                            httpErrorInterceptor: HttpErrorInterceptor,
                            httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {

        val client = OkHttpClient.Builder()
                .addInterceptor(acceptDialogInterceptor)
                .addInterceptor(userAgentInterceptor)
                .addInterceptor(httpErrorInterceptor)
                .addInterceptor(httpLoggingInterceptor)

        if (BuildConfig.DEBUG) {
            client.addInterceptor(mockInterceptor)
        }

        return client.build()
    }
}
