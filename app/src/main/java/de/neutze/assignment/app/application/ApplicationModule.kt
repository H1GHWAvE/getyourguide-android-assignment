package de.neutze.assignment.app.application

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule
internal constructor() {

    @Provides
    internal fun provideApplication(app: AssignmentApplication): Application {
        return app
    }

}
