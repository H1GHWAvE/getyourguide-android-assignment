package de.neutze.assignment.app.application

import de.neutze.assignment.browse.injection.module.BrowseViewModelModule
import dagger.Module

@Module(includes = [(BrowseViewModelModule::class)])
class ViewModelModule
