package de.neutze.assignment.app.application

import dagger.Module
import de.neutze.assignment.browse.injection.module.BrowseDataModule

@Module(includes = [(BrowseDataModule::class)])
internal class DataModule
