package de.neutze.assignment.app.mock

import com.google.gson.Gson
import de.neutze.assignment.browse.domain.entities.Review
import de.neutze.assignment.browse.network.model.ReviewResponse
import okhttp3.*
import okio.Buffer
import timber.log.Timber
import java.io.IOException

class MockInterceptor : Interceptor {


    private val headerName = "AcceptDialog"
    private val headerValue = "application/json"
    private val message = "MockInterceptor"
    private val pathPost = "/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/post"

    //region Interceptor

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        originalRequest.method()
        val path = originalRequest.url().encodedPath()

        Timber.e("%s", originalRequest)
        Timber.e("%s", originalRequest.body())
        Timber.e(path)
        Timber.e(pathPost)
        Timber.e("%s", path == pathPost)

        return if (path == pathPost) {
            createResponse(originalRequest, 201)
        } else {
            chain.proceed(originalRequest)
        }
    }

    //endregion


    //region helper

    @Throws(IOException::class)
    private fun createResponse(originalRequest: Request, code: Int): Response =
            Response.Builder()
                    .request(originalRequest)
                    .header(headerName, headerValue)
                    .protocol(Protocol.HTTP_1_1)
                    .message(message)
                    .code(code)
                    .body(ResponseBody.create(
                            MediaType.parse("text/plain; charset=UTF-8"),
                            getResponseReview(originalRequest.body())
                    ))
                    .build()


    private fun getResponseReview(body: RequestBody?): String {
        val review = Gson().fromJson(requestBodyToString(body), ReviewResponse::class.java)

        review.reviewId = 42

        return Gson().toJson(review)
    }

    @Throws(IOException::class)
    private fun requestBodyToString(requestBody: RequestBody?): String {
        val buffer = Buffer()
        requestBody?.writeTo(buffer)
        return buffer.readUtf8()
    }

    //endregion

}
