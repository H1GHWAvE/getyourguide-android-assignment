package de.neutze.assignment.app.application;

import de.neutze.assignment.app.home.presentation.HomeActivityComponent;
import de.neutze.assignment.base.injection.module.ActivityModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                DataModule.class,
                DomainModule.class,
                NetworkModule.class,
                ViewModelModule.class,
        }
)
public interface ApplicationComponent {

    void inject(AssignmentApplication app);

    HomeActivityComponent createHomeActivityComponent(ActivityModule module);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(AssignmentApplication app);

        ApplicationComponent build();

    }

}
