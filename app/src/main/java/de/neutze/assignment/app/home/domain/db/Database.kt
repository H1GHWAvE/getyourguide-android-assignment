package de.neutze.assignment.app.home.domain.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import de.neutze.assignment.base.domain.converter.CalendarConverter
import de.neutze.assignment.browse.domain.db.ReviewDao
import de.neutze.assignment.browse.domain.entities.Review

@Database(
        entities = [(Review::class)],
        version = 1,
        exportSchema = false
)
@TypeConverters(CalendarConverter::class)
abstract class Database : RoomDatabase() {

    abstract fun reviewsDao(): ReviewDao

}
