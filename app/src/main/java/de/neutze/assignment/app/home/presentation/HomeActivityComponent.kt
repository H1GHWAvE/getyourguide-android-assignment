package de.neutze.assignment.app.home.presentation

import dagger.Subcomponent
import de.neutze.assignment.base.injection.module.ActivityModule
import de.neutze.assignment.base.injection.scope.ActivityScope
import de.neutze.assignment.browse.injection.component.BrowseFragmentComponent

@ActivityScope
@Subcomponent(modules = [(ActivityModule::class)])
interface HomeActivityComponent : BrowseFragmentComponent.BrowseComponentCreator {

    fun inject(homeActivity: HomeActivity)

}
