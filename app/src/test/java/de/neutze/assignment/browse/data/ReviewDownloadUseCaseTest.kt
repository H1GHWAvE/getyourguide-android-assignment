package de.neutze.assignment.browse.data

class ReviewDownloadUseCaseTest /** : CourierUnitTest() {

private val file = "wallet/data/wallets.json"
private val path = "api/mobile/wallets/17"
private val url = "https://de.hurrier.com"

// 1. setup conditions for test...
// 2. Execute code under test...
// 3. Make assertions on the result...

private lateinit var sessionRepository: SessionRepository

private lateinit var walletDao: WalletDao
private lateinit var journalDao: JournalDao
private lateinit var discrepancyDao: DiscrepancyDao

private lateinit var serviceBuilder: ServiceBuilder
private lateinit var retrofitBuilder: RetrofitBuilder

private lateinit var walletDownloadUseCase: WalletDownloadUseCase

private val executor = TestExecutor()
private val happyClient = getClient(
TestInterceptor(
code = 200,
url = url,
path = "/$path",
file = file
)
)
private val errorClient = getClient(
TestInterceptor(
code = 401,
url = url,
path = "/$path"
)
)
private val happyWalletRouter get() = createWalletRouter(happyClient)
private val errorWalletRouter get() = createWalletRouter(errorClient)

private val discrepanciesCaptor = argumentCaptor<List<Discrepancy>>()
private val journalCaptor = argumentCaptor<List<JournalEntry>>()
private val walletCaptor = argumentCaptor<Wallet>()

private val walletMapper: WalletMapper = WalletMapper(
ClearanceMapper(),
DiscrepanciesMapper(),
JournalMapper(),
TransactionMapper()
)

private var expectedStatus: NetworkStatus? = null


//region CourierUnitTest

@Before
@Throws(Exception::class)
override fun setUp() {
super.setUp()

MockitoAnnotations.initMocks(this)

discrepancyDao = mock { on { insertDiscrepancies(discrepanciesCaptor.capture()) }.doAnswer { } }
journalDao = mock { on { insertJournal(journalCaptor.capture()) }.doAnswer { } }
walletDao = mock { on { insertWallet(walletCaptor.capture()) }.doAnswer { } }

sessionRepository = mock {
on { authenticationToken } doReturn authorization
on { url } doReturn url
on { userId } doReturn userId
}
}

@After
@Throws(Exception::class)
fun tearDown() {
expectedStatus = null
}

//endregion


//region WalletDownloadUseCase

@Test
@Throws(Exception::class)
fun getHappyPath() {
expectedStatus = NetworkStatus.SUCCESS

walletDownloadUseCase = WalletDownloadUseCase(
discrepancyDao = discrepancyDao,
executor = executor,
journalDao = journalDao,
walletDao = walletDao,
walletMapper = walletMapper,
walletRouter = happyWalletRouter
)

walletDownloadUseCase.get({ verifyResult(it, expectedStatus) })


WalletAssertions.assert(walletCaptor.firstValue)
JournalAssertions.assert(journalCaptor.allValues.first())
DiscrepancyAssertions.assert(discrepanciesCaptor.allValues.first())
}

@Test
@Throws(Exception::class)
fun getErrorPath() {
expectedStatus = NetworkStatus.UNAUTHORIZED

walletDownloadUseCase = WalletDownloadUseCase(
discrepancyDao = discrepancyDao,
executor = executor,
journalDao = journalDao,
walletDao = walletDao,
walletMapper = walletMapper,
walletRouter = errorWalletRouter
)

walletDownloadUseCase.get({ verifyResult(it, expectedStatus) })
}

//endregion


//region helper

private fun createWalletRouter(happyClient: OkHttpClient): WalletRouter {
retrofitBuilder = RetrofitBuilder(happyClient, sessionRepository)
serviceBuilder = ServiceBuilder(retrofitBuilder)
return WalletRouter(serviceBuilder, sessionRepository)
}

//endregion

}

 **/
